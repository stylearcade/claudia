module.exports = async function (func) {
	while (true) {
		try {
			return await func()
		} catch (e) {
			if (e.code === 'ResourceConflictException') {
				console.log('waiting for resource to be released')
				await new Promise(resolve => setTimeout(resolve, 2000))
			} else {
				throw e
			}
		}
	}
}
